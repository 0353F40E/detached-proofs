# CHIP-2022-02 Detached Proofs for Bitcoin Cash

        Title: Detached Signatures for Bitcoin Cash
        First Submission Date: 2022-02-12
        Owners: bitcoincashautist (ac-A60AB5450353F40E)
                {imaginary_username?}
                {Jason Dreyzehner?}
        Type: Technical
        Layers: Consensus
        Status: DRAFT
        Current Version: 0.1
        Last Edit Date: 2022-02-12

## Contents

1. [Summary](#summary)
2. [Deployment](#deployment)
3. [Benefits](#benefits)
4. [Technical Description](#technical-description)
5. [Specification](#specification)
6. [Security Considerations](#security-considerations)
7. [Implementations](#implementations)
8. [Test Cases](#test-cases)
9. [Activation Costs](#activation-costs)
10. [Ongoing Costs](#ongoing-costs)
11. [Costs and Benefits Summary](#costs-and-benefits-summary)
12. [Risk Assessment](#risk-assessment)
13. [Evaluation of Alternatives](#evaluation-of-alternatives)
14. [Discussions](#discussions)
15. [Statements](#statements)
16. [Changelog](#changelog)
17. [Copyright](#copyright)
18. [Credits](#credits)

## Summary

[[Back to Contents](#contents)]

This proposal describes an upgrade to Bitcoin Cash peer-to-peer electronic cash system that will, in a non-breaking way, allow Bitcoin Cash transactions to compress input unlocking scripts when building TXID preimages.
This will enable a whole new class of Script contracts that could rely on the feature to prove their ancestry by constructing fixed-size inductive proofs.

This will be accomplished by introducing a new input field to allow inputs to signal that they are storing a hash of the input's unlocking script instead of the locking script, while the locking scripts will come after the transaction body.
Such transaction will still be seen as properly formatted transaction by non-node software, containing the costs of upgrade to nodes.

## Deployment

[[Back to Contents](#contents)]

This proposal targets May 2023 activation.

It complements another two proposals, part of "PMv3+" upgrade strategy:

- [CHIP-2021-02 Unforgeable Groups for Bitcoin Cash](#);
- [CHIP-2022-01 Detached Signatures for Bitcoin Cash](#).

## Benefits

Many token and covenant designs can be supported within the existing Bitcoin Cash VM design and opcode limits, but one unintentional limitation is resistant to workarounds: the [quadratically increasing size of inductive proofs](https://blog.bitjson.com/hashed-witnesses-build-decentralized-applications-on-bitcoin-cash/). With fixed-size inductive proofs, contract designers can create SPV-compatible (Simplified Payment Verification) and contract-validated tokens, token-issuing covenants, and covenants which interact with each other.

### Synergistic Benefits

Covenants enabled by this proposal could emit native tokens, enabling DeFi applications while letting tokens be "free" P2PKH citizens.

Any P2PKH output, native token or BCH, will be able to enter a covenant enabled by this proposal, and token information will be accessible through introspection opcodes.
Therefore, interaction will be possible between such contracts and native tokens.

[[Back to Contents](#contents)]

## Technical Description

[[Back to Contents](#contents)]

This proposal is a spin-off from [CHIP-2021-01-PMv3: Version 3 Transaction Format] to enable a less complex variant of the "detached proofs" feature described there, and in a way that would not break node-dependent software.

### Detached Proofs FORMAT

We will extend the input format using the non-breaking [**P**re**F**i**X**](https://gitlab.com/bitcoin-cash-node/announcements/-/blob/master/2021-12-23_evaluate_viability_of_transaction_format_or_id_change_EN.md#an-alternative-to-breaking-change) method.

Full transaction format, including fields proposed by other 2 CHIPs:

* version, 4-byte uint
* input count, [compact variable length integer](https://reference.cash/protocol/formats/variable-length-integer)
* transaction inputs
    * input 0
        * previous output transaction hash, 32 raw bytes
        * previous output index, 4-byte uint
        * unlocking script length, [compact variable length integer](https://reference.cash/protocol/formats/variable-length-integer)
        * unlocking script
            * OPTIONAL: PFX_GROUP_GENESIS, 1-byte constant `0xEE`
                * group genesis version, 1-byte `0x01`
                * group genesis nonce, 2-byte
                * group type, 1-byte
                * group amount, 8-byte uint`
            * OPTIONAL: PFX_SIGNATURES, 1-byte constant `0xEF`
                * signature 0 length, [compact variable length integer](https://reference.cash/protocol/formats/variable-length-integer)
                * signature 0, variable number of raw bytes
                * ...
                * signature N length
                * signature N
            * **OPTIONAL: PFX_DETACHED_PROOF**, 1-byte constant `0xF0`
            * **IF PFX_DETACHED_PROOF**
                * H(real unlocking script), 32 raw bytes
            * ELSE
                * real unlocking script, variable number of raw bytes 
        * sequence number, 4-byte uint
    * ...
    * input N
* output count, [compact variable length integer](https://reference.cash/protocol/formats/variable-length-integer)
* transaction outputs
    * output 0
        * satoshi amount, 8-byte uint
        * locking script length, [compact variable length integer](https://reference.cash/protocol/formats/variable-length-integer)
        * locking script
            * OPTIONAL: PFX_GROUP, 1-byte constant `0xEE`
                * group ID, 32 raw bytes
                * group amount, 8-byte uint
            * real locking script, variable number of raw bytes
    * ...
    * output N
* lock-time, 4-byte uint
* **OPTIONAL: detached proofs**
    * proof 0
        - real unlocking script length, [compact variable length integer](https://reference.cash/protocol/formats/variable-length-integer)
        - real unlocking script, variable number of raw bytes
    * ...
    * proof N

The `detached proofs` block will be excluded from TXID preimage, and it will be omitted from relevant legacy API calls, which is discussed in more detail [here](#api-compatibility).

This way, unupgraded software, unaware of input and TX format change, will still interpret the TX as if it's a properly structured TX and will be able to verify its integrity.
The detached proof annotation will be interpreted as if part of the unlocking script.
As a consequence:

- Unupgraded *node* software would fork the blockchain because, from the point of view of unupgraded software, such unlocking script will be seen as starting with a disabled opcode.
- Unupgraded *non-node* software should already know how to deal with disabled opcodes found on the blockchain, so should not break when encountering them.
From its point of view `0xF0` could be some new data push opcode, followed by random data.

### Detached Proofs Consensus Rules

#### Transaction Validation

##### Transaction Integrity

Currently, the TXID is generated as:

- `TXID = H(preimageTXID)`,

where H is the double SHA-256 function and `preimageTXID = rawTransaction`.

The proposal would modify how the `preimageTXID` is constructed so that the above defined `detached proofs` block is truncated from the `rawTransaction` when building the `preimageTXID`.

##### Detached Proofs Integrity

If the PFX_DETACHED_PROOF is used with an input, then the related proof must be present in the `detached proofs` block.

The proofs can't have duplicates, must be canonically ordered by their hash, and each must be referenced by at least one input.

#### Script Virtual Machine

If the `PFX_DETACHED_PROOF` is used with an input, then the `real unlocking script` from the `detached proofs` block will be provided to Script VM instead of its hash.

Script virtual machine will never see the `PFX_DETACHED_PROOF` byte, and will simply execute the real unlocking script.

##### Introspection Opcodes

Existing introspection opcodes retrieving inputs unlocking script would return the related `real unlocking script` in HASH-256 byte order.

The Script VM wouldn't be able to tell whether an input has its locking script detached.

#### Signature Preimage Format

Input unlocking script is not part of the signature preimage therefore nothing needs changing.

If detached signatures are used, then `PFX_DETACHED_PROOF` and the hash of real unlocking script will naturally be included.

#### API Compatibility

The `getrawtransaction` must return the compressed raw TX instead of the uncompressed raw TX, i.e. it will return the `preimageTXID` defined above.

New API must be introduced to allow access to the uncompressed raw TX.

This way, backwards compatibility is fully preserved because old software will be able to verify TXIDs of transactions fed to it by upgraded node software, and the `preimageTXID` would look like a well-structured TX, albeit its `unlockingScript` wouldn't make sense to unupgraded software.

Old software wouldn't be able to validate input signatures as it would only be able to see the compressed input unlocking script.

## Specification

[[Back to Contents](#contents)]

{TODO}

## Security Considerations

[[Back to Contents](#contents)]

{TODO}

## Implementations

[[Back to Contents](#contents)]

{TODO}

## Test Cases

[[Back to Contents](#contents)]

{TODO}

## Activation Costs

[[Back to Contents](#contents)]

{TODO}

Contained to nodes.

## Ongoing Costs

[[Back to Contents](#contents)]

{TODO}

Contained to nodes.

## Costs and Benefits Summary

[[Back to Contents](#contents)]

{TODO}

## Risk Assessment

[[Back to Contents](#contents)]

{TODO}

## Evaluation of Alternatives

[[Back to Contents](#contents)]

{TODO}

## Discussions

[[Back to Contents](#contents)]

https://bitcoincashresearch.org/t/brainstorming-interaction-between-group-pmv3-and-introspection/430/18#detached-proofs-roll-out-4

## Statements

[[Back to Contents](#contents)]

## Changelog

[[Back to Contents](#contents)]

{TODO}

## Copyright

[[Back to Contents](#contents)]

To the extent possible under law, this work has waived all copyright and related or neighboring rights to this work under [CC0](https://creativecommons.org/publicdomain/zero/1.0/).

## Credits

[[Back to Contents](#contents)]

- Jason Dreyzehner, for [PMv3 proposal](https://github.com/bitjson/pmv3#chip-2021-01-pmv3-version-3-transaction-format) from which this proposal was spun off.
- Calin Culianu, for coming up with the [PreFiX](https://bitcoincashresearch.org/t/chip-2021-02-group-tokenization-for-bitcoin-cash/311/22) approach while discussing another proposal.

